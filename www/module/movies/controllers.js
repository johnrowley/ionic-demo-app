angular.module('movie.controllers', [])
.controller('MovieCtrl', function($scope, MovieService) {
	
	  $scope.title ="Tracker List"
	  $scope.trackedItems = MovieService.load();
	  
	  
	  
})
.controller('MovieDetailCtrl', function($scope, $stateParams, MovieService) {
	
  $scope.movie = MovieService.get($stateParams.movieId);
  
  
})
