angular.module('tracker.services', [])
.factory('TrackerService', function() {
	
	// Some fake testing data
	  var trackedItems = [{
	    id: 0,
	    title: 'Tracked Item 0',
	    description: 'This is description 0',
	    imgthumb: 'img/21490.jpg'
	  }, {
	    id: 1,
	    title: 'Tracked Item 1',
	    description: 'This is description 1',
	    imgthumb: 'img/26250.jpg'
	  }, {
	    id: 2,
	    title: 'Tracked Item 2',
	    description: 'This is description 2',
	    imgthumb: 'img/23367.jpg'
	  }, {
	    id: 3,
	    title: 'Tracked Item 3',
	    description: 'This is description 3',
	    imgthumb: 'img/1406tb.jpg'
	  }, {
	    id: 4,
	    title: 'Tracked Item 4',
	    description: 'This is description 4',
	    imgthumb: 'img/1406tb.jpg'
	  }];

	  return {
	    all: function() {
	      return trackedItems;
	    },
	    
	    add: function(trackedItem) {
	    	trackedItems.push(trackedItem)
		},
	    
	    remove: function(trackedItem) {
	    	trackedItems.splice(trackedItems.indexOf(trackedItem), 1);
	    },
	    get: function(trackedItemId) {
	      for (var i = 0; i < trackedItems.length; i++) {
	        if (trackedItems[i].id === parseInt(trackedItemId)) {
	          return trackedItems[i];
	        }
	      }
	      return null;
	    }
	  };
	
	
	
});