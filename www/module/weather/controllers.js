angular.module('weather.controllers', [])
.controller('WeatherCtrl', function($scope, WeatherService) {
	
	  $scope.title ="Weather List"
	  $scope.trackedItems = WeatherService.load();
	  
	  
	  
})
.controller('WeatherDetailCtrl', function($scope, $stateParams, WeatherService) {
	
  $scope.weather = WeatherService.get($stateParams.weatherId);
  
  
})
