// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 
                           
                           'starter.controllers',
                           'starter.services',
                           'starter.controllers.login',
                           'starter.services.auth',
                           'tracker.controllers',
                           'tracker.services',
                           'fb.tracker.controllers',
                           'fb.tracker.services',
                           'fb.camera.controllers',
                           'movie.controllers',
                           'movie.services',
                           'weather.controllers',
                           'weather.services',
                           'ngMessages',
                           'ngCordova',
                           'ngMap',
                           'firebase'])

.run(function($ionicPlatform, $rootScope, $state) {
	
	$rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
	    // We can catch the error thrown when the $requireSignIn promise is rejected
	    // and redirect the user back to the home page
	    if (error === "AUTH_REQUIRED") {
	    	console.log("login required")
	      $state.go("login");
	    }
	  });
	
	
	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

.state('login', {
              url: '/login',
              templateUrl: 'module/user/login.html',
              controller: 'LoginCtrl',
})

.state('tab.signout', {
              url: '/signout',
              views: {
                  'tab-signout': {
                   // templateUrl: 'module/user/login.html',
                    controller: 'SignoutCtrl',
                   
                  }
                }
})
  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl',
        resolve : { 
      	  currentAuth :  function (Auth) {
      		  
      		   return Auth.$requireSignIn();
      		  
      	  } // currentAuth
      	  
      	  
        } // resolve
      }
    }
  })

  .state('tab.tracker', {
      url: '/tracker',
      views: {
        'tab-tracker': {
          templateUrl: 'module/tracker/tab-tracker.html',
          controller: 'TrackerCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
      .state('tab.addTrackedItem', {
      url: '/addTrackedItem',
      views: {
        'tab-tracker': {
          templateUrl: 'module/tracker/add-trackedItem.html',
          controller: 'AddTrackedItemCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
    
      .state('tab.movies', {
      url: '/movies',
      views: {
        'tab-movies': {
          templateUrl: 'module/movies/tab-movies.html',
          controller: 'MovieCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
        
       .state('tab.movieDetail', {
      url: '/moviedetail/:movieId',
      views: {
        'tab-movies': {
          templateUrl: 'module/movies/item-detail.html',
          controller: 'MovieDetailCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
       .state('tab.weather', {
      url: '/weather',
      views: {
        'tab-weather': {
          templateUrl: 'module/weather/tab-weather.html',
          controller: 'WeatherCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
        
       .state('tab.weatherDetail', {
      url: '/weatherdetail/:weatherId',
      views: {
        'tab-weather': {
          templateUrl: 'module/weather/item-detail.html',
          controller: 'WeatherDetailCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
    
    
  .state('tab.fbtracker', {
      url: '/fbtracker',
      views: {
        'tab-fbtracker': {
          templateUrl: 'module/fbtracker/tab-tracker.html',
          controller: 'FBTrackerCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
      .state('tab.addTrackedItemToFB', {
      url: '/addTrackedItemToFB',
      views: {
        'tab-fbtracker': {
          templateUrl: 'module/fbtracker/add-trackedItem.html',
          controller: 'AddTrackedItemToFBCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
       .state('tab.fbItemDetail', {
      url: '/fbItemDetail/:fbItemId',
      views: {
        'tab-fbtracker': {
          templateUrl: 'module/fbtracker/item-detail.html',
          controller: 'fbItemDetailCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
   .state('tab.photos', {
      url: '/photos',
      views: {
        'tab-photos': {
          templateUrl: 'module/camera/tab-camera.html',
          controller: 'FBCameraCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })   
    .state('tab.addTrackedPhotoItemToFB', {
      url: '/addTrackedPhotoItemToFB',
      views: {
        'tab-photos': {
          templateUrl: 'module/camera/add-trackedItem.html',
          controller: 'AddTrackedPhotoItemToFBCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    }) 
    
           .state('tab.fbPhotoItemDetail', {
      url: '/fbPhotoItemDetail/:fbItemId',
      views: {
        'tab-photos': {
          templateUrl: 'module/camera/item-detail.html',
          controller: 'fbPhotoItemDetailCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		 
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    
    
  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        		  /*
        		  Auth.$onAuthStateChanged(function(user) {
        			 
        			  if (user) {
          			    console.log("resolved", user)
          			    return true;
          			  } else {
          			   console.log("user not signed in")
          			   return false;
          			  }
        			  
        		  })//Auth.on
        		  */
        		  
        		  
        	  } // currentAuth
        	  
        	  
          } // resolve
          
          
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl',
        resolve : { 
      	  currentAuth :  function (Auth) {
      		   return Auth.$requireSignIn();
     		  
      	  } // currentAuth
      	  
      	  
        } // resolve
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

});
