angular.module('tracker.controllers', [])
.controller('TrackerCtrl', function($scope, TrackerService) {
	
	  $scope.title ="Tracker List"
	  $scope.trackedItems = TrackerService.all();
	  
	  
	  
})

.controller('AddTrackedItemCtrl', function($scope, $state, TrackerService) {
	
	console.log("AddTrackedItemCtrl::addTrackerItem");
	
	
	//http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
	
	$scope.generateUUID = function (){
	    var d = new Date().getTime();
	    if(window.performance && typeof window.performance.now === "function"){
	        d += performance.now(); //use high-precision timer if available
	    }
	    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	        var r = (d + Math.random()*16)%16 | 0;
	        d = Math.floor(d/16);
	        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
	    });
	    return uuid;
	}
	
	
	
	var uniqueId = $scope.generateUUID();
	
	
	
	$scope.trackerMasterItem = {id : uniqueId, title : "", description : "",imgthumb : 'img/max.png'};
	
	$scope.trackerItem = {id : uniqueId, title : "", description : "",imgthumb : 'img/max.png'};
	
	$scope.addTrackerItem = function (form) {
		
		if (form.$valid) {
			
			//console.log("TrackerCtrl::addTrackerItem");
			
			
			TrackerService.add(angular.copy($scope.trackerItem))
			//Reset and get new id for master 
			$scope.trackerMasterItem.id = $scope.generateUUID();
			//Reset trackerItem
			$scope.addTrackerItem = angular.copy($scope.trackerMasterItem);
			
			
			$state.go("tab.tracker")
			
		} else {
			
			console.log("TrackerCtrl::invalidForm");
			console.log(form.$error);
		}
	}
	
})
