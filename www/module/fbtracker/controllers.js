angular.module('fb.tracker.controllers', [])
.controller('FBTrackerCtrl', function($scope, $firebaseArray ) {
	
	  $scope.title ="FB Tracker List"
		  
		  
	  var ref = firebase.database().ref().child("trackedItems");
	  // create a synchronized array
	  // click on `index.html` above to see it used in the DOM!
	  $scope.trackedItems = $firebaseArray(ref);  
	  
	  console.log($scope.trackedItems);
	  
	  
})

.controller('AddTrackedItemToFBCtrl', function($scope, $state, $cordovaGeolocation,  $ionicPlatform, $firebaseArray) {
	
	$scope.coords = { latitude : 53.330844799999994, longitude : -6.2298736};
	
	$scope.trackerMasterItem = {title : "", description : "",imgthumb : 'img/max.png', latitude : $scope.coords.latitude, longitude : $scope.coords.longitude};
	
	$scope.trackerItem = {title : "", description : "",imgthumb : 'img/max.png', latitude : $scope.coords.latitude, longitude : $scope.coords.longitude};
	
	$ionicPlatform.ready(function() {
		  var posOptions = {timeout: 10000, enableHighAccuracy: true};
		  $cordovaGeolocation.getCurrentPosition(posOptions)
		    .then(function(position) {
		    	$scope.coords = position.coords;
		    	
		    	$scope.trackerItem.latitude = $scope.coords.latitude;
		    	
		    	$scope.trackerItem.longitude = $scope.coords.longitude;
		    	
		   // showMap(position.coords);
		    }, function(err) {
		    	console.log('getCurrentPosition error: ' + angular.toJson(err));
		    });
		});
		
		
	
	
	
	
	
	console.log("AddTrackedItemCtrl::addTrackerItem");
	
	
	
	var ref = firebase.database().ref().child("trackedItems");
	  // create a synchronized array
	 $scope.trackedItems = $firebaseArray(ref);
	
	
	$scope.addTrackerItem = function (form) {
		
		if (form.$valid) {
			
			//console.log("TrackerCtrl::addTrackerItem");
			  
			$scope.trackedItems.$add(angular.copy($scope.trackerItem));
		
			//Reset trackerItem
			$scope.trackerItem = angular.copy($scope.trackerMasterItem);
			
			
			$state.go("tab.fbtracker")
			
		} else {
			
			console.log("TrackerCtrl::invalidForm");
			console.log(form.$error);
		}
	}
	
})

.controller('fbItemDetailCtrl',function ($scope, $stateParams, $firebaseObject,$cordovaGeolocation, $ionicPlatform) {
	
	
	$scope.$on('mapInitialized', function (event,map) {

        $scope.map = map;

    });


	$ionicPlatform.ready(function() {
	  var posOptions = {timeout: 10000, enableHighAccuracy: true};
	  $cordovaGeolocation.getCurrentPosition(posOptions)
	    .then(function(position) {
	    	$scope.coords = position.coords;
	   // showMap(position.coords);
	    }, function(err) {
	    	console.log('getCurrentPosition error: ' + angular.toJson(err));
	    });
	});

	$scope.id = $stateParams.fbItemId;
	//$scope.item = {}
	
//	$scope.profile = $firebaseObject(ref.child('profiles').child('physicsmarie'));
	
	$scope.item = $firebaseObject(firebase.database().ref().child("trackedItems").child($stateParams.fbItemId));

	

});